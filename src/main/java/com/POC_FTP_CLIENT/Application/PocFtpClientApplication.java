package com.POC_FTP_CLIENT.Application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class PocFtpClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocFtpClientApplication.class, args);
	}

}
