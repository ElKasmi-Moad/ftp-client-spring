package com.POC_FTP_CLIENT.Application.controller;


import com.POC_FTP_CLIENT.Application.FtpService.FTPFileWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.io.*;


@RestController
@RequestMapping("/ftp")
public class FTPController {

    @Autowired
    FTPFileWriter ftpFileWriter;

    File firstLocalFile = new File("C:/Users/moad el kasmi/Desktop/FTPtest.txt");
    String firstRemoteFile = "FTPtest.txt";

    String filename = "testText.jpg";

    InputStream inputStream;




    @GetMapping("/upload")
    public String upload() throws Exception{

        System.out.println("Start uploading first file");

        {
            try {
                inputStream = new FileInputStream(firstLocalFile);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }


        boolean done = ftpFileWriter.saveFile( inputStream, firstRemoteFile, false);
        inputStream.close();

        if (done) {
            System.out.println("The first file is uploaded successfully.");
            ftpFileWriter.close();

            return "Upload Success" ;
        }
        else {
            ftpFileWriter.close();
            return "Upload Fail";

        }
    }


    @GetMapping("/download")
    public String download() throws Exception{
        System.out.println("Start downloading first file");
        OutputStream outputStream = new FileOutputStream(filename);
        boolean done = ftpFileWriter.loadFile( "/Effyis.jpg",outputStream );
        outputStream.close();
        if (done) {
            ftpFileWriter.close();
            System.out.println("The first file is Downloaded successfully.");
            return "Download Sucess";
        }
        else{
            ftpFileWriter.close();
            return "Download Fail";
        }
    }



    @GetMapping("/uploadLocalFile")
    public String uploadLocalFile() throws Exception{
        System.out.println("Start uploading second file");
        ftpFileWriter.open();
        if(ftpFileWriter.isConnected()){

            ftpFileWriter.saveFile(firstRemoteFile,"/" ,true);
            System.out.println("The first file is uploaded successfully.");
            ftpFileWriter.close();
            return "Upload Success";
        }
        else {
            ftpFileWriter.close();
            return "Upload Failed";
        }
    }
}


